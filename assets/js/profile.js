function logout(){  
    clearCookie('role');                                                                                                                                  
    clearCookie('logged_in');
    clearCookie('email_verified');
    clearCookie('otp_validate');
    clearCookie('authentication');
    clearCookie('pipeline_authentication');
    clearCookie('selected_image');
    clearCookie('selected_image_url'); 
    window.location.href = "sign-in.html";         
}