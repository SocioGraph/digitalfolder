let base_url = "https://d3nxn5bl526zn7.cloudfront.net/merino_digitalfolder/temp_renders/";
if ( window.location.origin.indexOf('merinolaminates.com') >= 0 ) {
    base_url = "https://d3nxn5bl526zn7.cloudfront.net/merino_digitalfolder/renders/";
} else if ( window.location.origin.indexOf('localhost') >= 0 ) {
    base_url = "https://dashboard.iamdave.ai/static/uploads/blender/assets/merino_digitalfolder/";
}
let suffix = 'webp'
let options = {};
let room_id = makeSingle(get_url_params()['room_id'] || 'rs_master_bedroom');
let view_id = makeSingle(get_url_params()['view_id']);
let surface_id = null;
let image_list = null;
let product_id = get_url_params()['product_id'] || getCookie('product_id') || null;
let application = get_url_params()['application'] || getCookie('application') || null;
let default_finish = 'Suede';
let product_finish = get_url_params()['product_finish'] || default_finish;
const getOptions = (rooms) => {
    try 
    {
        for (k of rooms) {
            options[k['id']] = {
                id: k['room_id'] || k['id'],
                name: k['title']
            }
            for (j of k['views'] ) {
                options[k['id']][j['id']] = {
                    id: j['view_id'] || j['id'],
                    name: k['title'] + ' - ' + j['title'],
                    background: j['background-color'],
                    horizontal: j['horizontal'] || false,
                }
                for (i of j['surfaces'] ) {
                    options[k['id']][j['id']][i['id']] = i;
                    i['id'] = i['surface_id'] || i['id']
                }
            }
        }
        if ( !view_id || !options[room_id][view_id] || view_id == 'id' || view_id == 'name' || view_id == 'background' || view_id == 'horizontal' ) {
            for (const k of Object.keys(options[room_id])) {
                if (k != 'id' && k != 'name' && k != 'background' && k != 'horizontal') {
                    view_id = k;
                    break
                }
            }
            set_url_params({"view_id": view_id});
        }
        return options
    } catch (error) {
        console.log("Error : ", error);
    } 
}
const getVizRooms = async () => {
    try 
    {
        const roomApiResponse = await fetch("assets/json/applicationScenes.json");
        const rooms = await roomApiResponse.json();   
        console.debug(rooms);
        getOptions(rooms);
        $( "#viz_room_name" ).text(options[room_id][view_id].name);
        image_list = await init_image_list(room_id, view_id);
        let ind = true;
        Object.entries(options[room_id][view_id]).forEach(function(s, k) {
            if (s[0] == 'id' || s[0] == 'name' || s[0] == 'background' || s[0] == 'horizontal') {
                return false
            }
            if (s[1]["variable"]) {
                let vw = $( "#main" ).width();
                let iw = ($( window ).width() > $( window ).height() ? $( window ).height() : $( window ).width())*0.05
                $("#main").append(`
                <img src=${ind ? "assets/images/otag.webp" :  "assets/images/tag.webp"} id="${s[0]}" data-left="${s[1]["left"]}" data-top="${s[1]["top"]}" style="height:${iw}px;position:absolute;">
              `);
                arrange_tags();
                $("#" + s[0]).data("room_id", room_id);
                $("#" + s[0]).data("view_id", view_id);
                $("#" + s[0]).data("surface_id", s[0]);
                $("#" + s[0]).data("image_list", image_list);
                $("#" + s[0]).on('click', function() {
                    $( "#main img").attr('src', 'assets/images/tag.webp');
                    $( this ).attr('src',  "assets/images/otag.webp");
                    surface_id = $( this ).data('surface_id')
                    set_url_params({"surface_id": surface_id});
                });
                if ( ind ) {
                    surface_id = s[0];
                    set_url_params({"surface_id": surface_id});
                }
                ind = false;
            }
        });
        return options;
    } catch (error) {
        console.log("Error : ", error);
    }
}
function get_other_views() { 
    return Object.entries(options[room_id]).filter(function(s) {
        if (s[0] == 'id' || s[0] == 'name' || s[0] == 'background' || s[0] == 'horizontal') {
            return false;
        }
        return true;
    }).map(function(s) {
        return {
          'room_id': room_id,
          'view_id': s[0],
          'active': (s[0] == view_id)
        }
    });
}

function load_other_view(vid, rid) {
    vid = vid || view_id;
    rid = rid || room_id;
    set_url_params({'room_id': rid, 'view_id': vid}, true);
}
function init_image_list(room_id, view_id, selection, identifier, dont_set_urls) {
    let ind = 0;
    let dind = 0;
    room = options[room_id]['id']
    view = options[room_id][view_id]['id']
    let image_list = {};
    selection = selection || {};
    Object.entries(options[room_id][view_id]).map(function (v, k) {
        if (v[0] == 'id' || v[0] == 'name' || v[0] == 'background' || v[0] == 'horizontal' ) {
            return false;
        }
        ind = ind + 1;
        let s = null;
        if (v[1]['variable']) {
            if ( product_id && ( !v[1]['horizontal'] || !application || makeSingle(application).split(',').indexOf('HORIZONTAL') >=0 ) ) {
                s = `${room}_${view}_${v[1]['surface_id']||v[1]['id']}_${product_id}_${product_finish||default_finish}`.replace(/_+/g, '_').replace(/^_|_$/g, '');
                let prms = {}
                prms[v[0]] = product_id;
                prms[`${v[0]}.finish`] = product_finish || default_finish;
                prms['product_id'] = null;
                if ( $.isEmptyObject( selection ) && !dont_set_urls ) {
                    set_url_params(prms);
                }
            } else {
                let init = selection[v[0]] || get_url_params()[v[0]] || v[1]["init"];
                let finish = toFinishType(selection[`${v[0]}.finish`] || get_url_params()[`${v[0]}.finish`] || v[1]["finish"] || default_finish) ;
                s = `${room}_${view}_${v[1]['surface_id']||v[1]['id']}_${init}_${finish}`.replace(/_+/g, '_').replace(/^_|_$/g, '');
                let prms = {}
                prms[v[0]] = init;
                prms[`${v[0]}.finish`] = finish;
                if ( $.isEmptyObject( selection ) && !dont_set_urls ) {
                    set_url_params(prms);
                }
            }
        } else {
            s = `${room}_${view}_${v[0]}`.replace(/_+/g, '_').replace(/^_|_$/g, '');
        }
        s = `${base_url}${s}.${suffix}`;
        $('<img crossorigin="anonymous">')
            .attr("src", s)
            .on("load", function () {
                dind = dind + 1;
                if (dind >= ind) {
                    $(".loader").hide();
                }
            });
        image_list[v[0]] = s;
        return s;
    });
    product_id = null;
    set_image(image_list, identifier);
    if ( options[room_id][view_id].background ) {
        $( identifier || "#main").css("background-color", options[room_id][view_id].background);
    }
    return image_list;
}
function set_image(image_list, identifier) {
    identifier = identifier || "#main";
    let mil = Object.values(image_list).map(function (v) {
            return "url(" + v + ")";
        }).join(",");
    //console.log(mil);
    $( identifier ).css("background-image", mil);
}

function change(design_id, finish, application) {
    finish = toFinishType(finish || default_finish);
    $(".loader").show();
    room = options[room_id]['id']
    view = options[room_id][view_id]['id']
    surface = options[room_id][view_id][surface_id]['id']
    horizontal = options[room_id][view_id][surface_id]['horizontal']
    if ( application && horizontal && application.split(',').indexOf('HORIZONTAL') < 0 ) {
        return false
    }
    let e = `${room}_${view}_${surface}_${design_id}_${finish}`.replace(/_+/g, '_').replace(/^_|_$/g, '');
    e = `${base_url}${e}.${suffix}`
    console.log("Changed image path: " + e);
    $('<img crossorigin="anonymous">')
        .attr("src", e)
        .on("load", function () {
            image_list[surface_id] = e;
            set_image(image_list);
            let prms = {}
            prms["surface_id"] = surface_id;
            prms[surface_id] = design_id;
            prms[`${surface_id}.finish`] = finish;
            set_url_params(prms);
            $(".loader").hide();
        });
    add_interaction({
        "product_id": makeSingle(design_id),
        "room_id": room_id,
        "view_id": view_id,
        "surface_id": surface_id,
        "room_name": options[room_id]['name'],
        "view_name": options[room_id][view_id]['name'],
        "surface_name": options[room_id][view_id][surface_id]['name'],
        "finish": finish,
        "_async": true
    });
    return true
}
async function set_up_room() {
    $(".loader").show();
    $("#main").empty();
    await getVizRooms();
    if ( typeof( set_up_other_views ) != 'undefined' ) {
        set_up_other_views(get_other_views());
    }
    return image_list;
}

function show_sprites() {
  $("#main img").show();
}
function hide_sprites() {
  $("#main img").hide();
}
function arrange_tags() {
    let vw = $( "#main" ).width()
    let iw = ($( window ).width() > $( window ).height() ? $( window ).height() : $( window ).width())*0.05
    $( "#main img" ).each(function() {
        let lf = $( this ).attr('data-left');
        let tp = $( this ).attr('data-top');
        $( this ).css('height', `${iw}px`)
        $( this ).css('left', `${(lf * vw) - iw/2 }px`);
        $( this ).css('top', `${( tp * 0.5132 * vw) - iw/2}px`);
    });
}
$( window ).on('resize', function() {
    arrange_tags();
});
