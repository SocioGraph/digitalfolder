let opened_filters = [];
let opened_modal = [];
let logged_in = getCookie("logged_in") || false;
let dave_url = "https://dashboard.iamdave.ai";
let enterprise_id = "merino_digitalfolder"
let signup_api_key = "9368547c-bd11-3c8e-bfa6-accf9d4a0643"
// device detection
let is_iPad = navigator.userAgent.match(/iPad/i) != null;
let is_IE = navigator.userAgent.match(/MSIE/i) != null;
function detectMob() {
    let details = navigator.userAgent;
    let regexp = /android|iphone|kindle|ipad/i;
    let isMobileDevice = regexp.test(details);
    if ( isMobileDevice ) {
        return true
    }
    let iw = $( window ).width() > $( window ).height() ? $( window ).height() : $( window ).width()
    if ( $( window ).width() < 540 ) {
        return true
    }
    return false
}

if(!detectMob()){
    $('#qrCodeModal').fadeIn();
}
    
$(window).on('load', function () {
    $('#mds_loader').hide();
})
 
function getCookie(key, dflt) {
    if (dflt === undefined) {
        dflt = null;
    }
    let result = window.localStorage.getItem(key);
    if ( result === null || result === undefined ) {
        return dflt;
    }
    try{
        r = JSON.parse(result);
    }catch(err){
        if(result) {
            r = result;
        }
    }
    return r;
}

function setCookie(key, value, hoursExpire) {
    if ( hoursExpire === undefined ) {
        hoursExpire = 24
    }
    if ( value === null || value === undefined ) {
        return;
    }
    if ( typeof value == "object" ) {
        value = JSON.stringify(value);
    }
    if ( hoursExpire < 0 ) {
        window.localStorage.removeItem(key);
    } else {
        window.localStorage.setItem(key, value);
    }
}

function clearCookie(key) {
    setCookie(key,"",-24);
}

function clearAllCookies() {
    clearCookie('role');
    clearCookie('logged_in');
    clearCookie('email_verified');
    clearCookie('last_login');
    clearCookie('otp_validate');
    clearCookie('authentication');
    clearCookie('pipeline_authentication');
    clearCookie('selected_image');
    clearCookie('selected_image_url');
}

function signup(data, postfunc, errorFunc){
    data = data || {};
    var signupurl=dave_url + "/customer-signup";
    //Password string generator
    var randomstring = Math.random().toString(36).slice(-8);
    let ddata = {
       "validated": true,
       "email": "ananth+"+generate_random_string(4)+"@i2ce.in",
       "name": randomstring,
       "password": generate_random_string(8)
    };
    for (let k in ddata) {
        if ( !data[k] ) {
            data[k] = ddata[k]
        }
    }
    let params = get_url_params();
    data['browser'] = '{agent_info.browser}';
    data['os'] = '{agent_info.os}';
    data['device_type'] = '{agent_info.device}';
    for ( k of ["utm_source", "utm_medium", "utm_campaign", "utm_term"] ) {
        if ( params[k] ) {
            data[k] = params[k];
        }
    }
    $.ajax({
        url: signupurl,
        method: "POST",
        dataType: "json",
        contentType: "json",
        withCredentials: true,
        headers:{
            "Content-Type":"application/json",
            "X-I2CE-ENTERPRISE-ID": enterprise_id,
            "X-I2CE-SIGNUP-API-KEY": signup_api_key
        },
        data: JSON.stringify(data),
        success: function(data) {
            HEADERS = {
                "Content-Type":"application/json",
                "X-I2CE-ENTERPRISE-ID": enterprise_id,
                "X-I2CE-USER-ID": data.customer_id,
                "X-I2CE-API-KEY": data.api_key
            }
            setCookie("authentication",JSON.stringify(HEADERS),24);
            $( "#loader" ).hide();
        },
        error: function(r, e) {
            console.log(e)
            $( ".loader" ).show();
            if (errorFunc && typeof ( errorFunc ) == 'function' ) {
                errorFunc(r, e);
            }
        }
    }).done(function(data) {
        // console.log(data)
        HEADERS = {
            "Content-Type":"application/json",
            "X-I2CE-ENTERPRISE-ID": enterprise_id,
            "X-I2CE-USER-ID": data.customer_id,
            "X-I2CE-API-KEY": data.api_key
        }
        setCookie("authentication",JSON.stringify(HEADERS), 24);
        $( ".loader" ).hide();
        if ( postfunc && typeof( postfunc ) == 'function') {
            postfunc(data);
        }
    });
}

function generate_random_string(string_length){
    let random_string = '';
    let random_ascii;
    for(let i = 0; i < string_length; i++) {
        random_ascii = Math.floor((Math.random() * 25) + 97);
        random_string += String.fromCharCode(random_ascii);
    }
    return random_string;
}

function generate_random_otp(string_length){
    string_length = string_length || 4;
    let random_string = '';
    let random_ascii;
    for(let i = 0; i < string_length; i++) {
        random_ascii = Math.floor((Math.random() * 10) + 48);
        random_string += String.fromCharCode(random_ascii);
    }
    return random_string;
}

function ajaxRequestSync(URL,METHOD,HEADERS,callbackFunc, errorFunc, async, timeout){
       timeout = timeout || 600000;
       $.ajax({
        url: URL,
        method: METHOD,
        dataType: "json",
        contentType: "json",
        async: async || false,
        timeout: timeout,
        headers:{
              "Content-Type":"application/json",
              "X-I2CE-ENTERPRISE-ID": enterprise_id,
              "X-I2CE-USER-ID":HEADERS['X-I2CE-USER-ID'],
              "X-I2CE-API-KEY":HEADERS['X-I2CE-API-KEY']
        },
        statusCode: {
            401: signup,
            504: errorFunc
        }
      }).done(function(data) {
          result=data;
          if (callbackFunc ) {
            callbackFunc(data);
          }
      }).fail(function(err) {
          if (errorFunc) {
              errorFunc(err)
          }
      });
}
function ajaxRequest(URL,METHOD,HEADERS,callbackFunc,errorFunc, timeout){
    return ajaxRequestSync(URL, METHOD, HEADERS, callbackFunc, errorFunc, true, timeout);
}

function merge_arrays(array1, array2) {
    if (!array1) {
        array1 = [];
    }
    if (!array2) {
        array2 = [];
    }
    for (var i = 0; i < array2.length; i++) {
        if (array1.indexOf(array2[i]) === -1) {
            array1.push(array2[i]);
        }
    }
    return array1
}

function ajaxRequestWithData(URL,METHOD,HEADERS,DATA,callbackFunc,errorFunc,unauthorized){
       var defaultData="" ;
       if(DATA){
        defaultData=DATA;
       }
       if(!unauthorized) {
           unauthorized = signup
       }
       $.ajax({
        url: URL,
        method: METHOD,
        dataType: "json",
        contentType: "application/json",
        headers:{
              "Content-Type":"application/json",
              "X-I2CE-ENTERPRISE-ID": enterprise_id,
              "X-I2CE-USER-ID":HEADERS['X-I2CE-USER-ID'],
              "X-I2CE-API-KEY":HEADERS['X-I2CE-API-KEY']
            },
        data:defaultData,
        statusCode: {
            401: unauthorized,
            404: unauthorized,
        }
      }).done(function(data) {
          if (callbackFunc ) {
            callbackFunc(data);
          }
      }).fail(function(err) {
          if (errorFunc) {
              errorFunc(err)
          }
      });

}        

function Trim(strValue) {
  return strValue.replace(/^\s+|\s+$/g, '');
}

// String manipulations
function toTitleCase(str) {
    if ( typeof( str ) != 'string' ) {
        return str;
    }
    return str.replace(/_/g, ' ').replace(/(?:^|\s)\w/g, function(match) {
        return match.toUpperCase();
    });
}
function toIdType(str, to_lower) {
    let r = (str||'').replace(/\+/g, '').replace(/\(/g,'').replace(/\)/g,'').replace(/\s/g,'_');
    if (to_lower) {
        return r.toLowerCase();
    }
    return r;
}
function toFinishType(str) {
    if ( typeof( str ) != 'string' ) {
        return str;
    }
    return (str||'').replace(/\//g, '_');
}
function toUrlType(str) {
    if ( typeof( str ) != 'string' ) {
        return str;
    }
    return (str||'').replace(/\+/g, '%2B');
}
function toQueryType(str) {
    str = str || '';
    if ( typeof( str ) != 'string' ) {
        return str;
    }
    if ( str.includes('+') ) {
        str = '~' + str.slice(0,str.indexOf('+'));
    }
    if ( str.includes(',') ) {
        return $.map(str.split(','), function(v) {return v.trim()})
    } 
    return str;
}
// String manipulations

function set_url_params(options, reload) {
    if ( window.URLSearchParams !== undefined ) {
        var searchParams = new URLSearchParams(window.location.search)
        let up = getCookie('url_params', {});
        let rid = options['room_id'] || searchParams.get('room_id');
        let vid = options['view_id'] || searchParams.get('view_id');
        if ( ( !rid || up['room_id'] == rid ) && ( !vid || up['view_id'] == vid ) ) {
            Object.entries(up).map(function(k, v) {
                if ( searchParams.get(k[0]) === null ) {
                    searchParams.set(k[0], k[1]);
                    return true;
                }
                return false
            });
        }
        Object.entries(options).map(function(k, v) {
            if ( k[1] === null ) {
                searchParams.delete(k[0]);
            } else {
                searchParams.set(k[0], k[1]);
            }
            return true;
        });
        var newRelativePathQuery = window.location.pathname + '?' + searchParams.toString();
        up = {};
        for (const [k, v] of searchParams.entries()) {
            up[k] = v;
        }
        setCookie('url_params', up);
        if ( !reload ) {
            history.replaceState(null, '', newRelativePathQuery);
        } else {
            window.location.replace(newRelativePathQuery);
        }
    }
}

function get_url_params(qd) {
    qd = qd || {};
    if (location.search) location.search.substr(1).split("&").forEach(function(item) {
        var s = item.split("="),
            k = s[0],
            v = s[1] && decodeURIComponent(s[1]); //  null-coalescing / short-circuit
        //(k in qd) ? qd[k].push(v) : qd[k] = [v]
        (qd[k] = qd[k] || []).push(v) // null-coalescing / short-circuit
    })
    qc = getCookie('url_params', {});
    for (const [k, v] of Object.entries(qc)) {
        if ( qd[k] == null || qd[k] == undefined ) {
            qd[k] = v;
        }
    }
    return qd;
}
function apply_url_params(identifier) {
}
function check_orientation() {
    var viewport = $('meta[name="viewport"]');
    if (screen.width <= 1240) {
        viewport.attr("content", "width=1240, user-scalable=no");
    } else {
        viewport.attr("content", "width=device-width, initial-scale=1, user-scalable=no");
    }
    if ( window.innerWidth < 640 ) {
        if ( window.innerHeight < 640 ) {
            swal({
                title: 'Incompatible Device',
                text: "This device does not seem compatible with this visualizer. Please use a larger screen to enjoy the experience.",
                icon: "assets/img/incompatible.jpg",
                button: false,
                closeOnClickOutside: false
            });
        } else {
            swal({
                icon: "assets/img/orientation.webp",
                title: 'Use Landscape View',
                text: "This device is not recommended for viewing the visualizer. If you still want to use it, please rotate your device to get a better view of the visualizer",
                button: false,
                closeOnClickOutside: false
            });
        }
    } else if ( window.innerWidth < window.innerHeight ) {
        swal({
            icon: "assets/img/orientation.webp",
            title: 'Use Landscape View',
            text: "Please rotate your device to get a better view of the visualizer",
            button: false,
            closeOnClickOutside: false
        });
    } else if (  isMobile && !shown_incompatible ) {
        swal({
            title: 'Please use a Larger Screen!',
            text: "Please use a desktop, laptop or a tab to enjoy the experience.",
            icon: "assets/img/incompatible.jpg",
        });
        shown_incompatible = true;
        setCookie("shown_incompatible", shown_incompatible);
    } else if ( is_IE ) {
        swal({
            title: 'Browser not Recommended',
            text: "This browser does not seem to be recommended with this visualizer. Please use a modern browser such as Chrome, Firefox or Safari.",
            icon: "assets/img/incompatible.jpg",
        });
    } else {
        try {
            swal.close();
        } catch (err) {
            console.log("Orientation seems to be fine");
        }
    }
}

function today() {
    var fullDate = new Date();
    console.log(fullDate);
    var twoDigitMonth = ("0" + (fullDate.getMonth()+1)).slice(-2);
    var twoDigitDate = ("0" + fullDate.getDate()).slice(-2);
    return fullDate.getFullYear() + "-" + twoDigitMonth + '-' + twoDigitDate;
}

function one_week_ago(num) {
    num = num || 1;
    var fullDate = new Date();
    fullDate.setDate(fullDate.getDate() - num * 7);
    var twoDigitMonth = ("0" + (fullDate.getMonth()+1)).slice(-2);
    var twoDigitDate = ("0" + fullDate.getDate()).slice(-2);
    return fullDate.getFullYear() + "-" + twoDigitMonth + '-' + twoDigitDate;
}

function one_month_ago(num) {
    num = num || 1;
    var fullDate = new Date();
    fullDate.setDate(fullDate.getDate() - num * 30);
    var twoDigitMonth = ("0" + (fullDate.getMonth()+1)).slice(-2);
    var twoDigitDate = ("0" + fullDate.getDate()).slice(-2);
    return fullDate.getFullYear() + "-" + twoDigitMonth + '-' + twoDigitDate;
}

function one_year_ago(num) {
    num = num || 1;
    var fullDate = new Date();
    fullDate.setDate(fullDate.getDate() - num*365);
    var twoDigitMonth = ("0" + (fullDate.getMonth()+1)).slice(-2);
    var twoDigitDate = ("0" + fullDate.getDate()).slice(-2);
    return fullDate.getFullYear() + "-" + twoDigitMonth + '-' + twoDigitDate;
}

function this_quarter(quarters) {
    function dm(m) {
        let y = fullDate.getFullYear() 
        while (m < 0 ) {
            m = m + 12;
            y = y - 1;
        }
        while (m >= 12 ) {
            m = m - 12;
            y = y + 1
        }
        m = 1 + (parseInt(m/3)*3);
        m = ("0" + m).slice(-2);
        return y + "-" + m + "-01"
    }
    quarters = quarters || 0;
    let fullDate = new Date();
    let m1 = fullDate.getMonth() - (quarters*3);
    let m2 = m1 + 4;
    return dm(m1) + "," + dm(m2);
}

function of_month(month) {
    let fullDate = new Date();
    let y1 = fullDate.getFullYear();
    while (month < 0) {
        month = month + 12;
        y1 = y1 - 1;
    }
    while (month >= 12) {
        month = month - 12;
        y1 = y1 + 1;
    }
    if (month > fullDate.getMonth() ) {
        y1 = y1 - 1;
    }
    let y2 = y1;
    let m1 = ("0" + (month+1)).slice(-2);
    let m2 = ("0" + (month+2)).slice(-2);
    if ( month == 11 ) {
        y2 = y1 + 1;
        m2 = "01"
    }
    return y1 + "-" + m1 + '-01,' + y2 + "-" + m2 + '-01'
}


function get_customer(params, callbackFunc, errorFunc, headersfromcookie) {
    params = params || {};
    headersfromcookie = headersfromcookie || getCookie('authentication')
    let f = function() {
        ajaxRequestWithData(
            dave_url + "/get/customer/" + (params.customer_id || headersfromcookie['X-I2CE-USER-ID']), 
            'GET', headersfromcookie, 
            params,
            callbackFunc,
            errorFunc
        )
    }
    if (!headersfromcookie) {
        signup({}, f);
        return
    }
    f();
}

function update_customer(params, callbackFunc, errorFunc, headersfromcookie) {
    params = params || {};
    if ( params.name && params.email && params.phone_number ) {
        setCookie("user_details", {'name': params.name, 'email': params.email, 'phone_number': params.phone_number})
    }
    headersfromcookie = headersfromcookie || getCookie('authentication')
    let f = function() {
        ajaxRequestWithData(
            dave_url + "/update/customer/" + (params.customer_id || headersfromcookie['X-I2CE-USER-ID']), 
            'POST', headersfromcookie, 
            JSON.stringify(params),
            callbackFunc,
            errorFunc
        )
    }
    if (!headersfromcookie) {
        signup({}, f);
        return
    }
    f();
}

function add_interaction(params, callbackFunc, errorFunc, headersfromcookie) {
    headersfromcookie = headersfromcookie || getCookie('authentication')
    params = params || {};
    params["customer_id"] = headersfromcookie['X-I2CE-USER-ID'];
    if ( !params.stage ) {
        params['stage'] = 'viewed';
    }
    let cstage = ( params.stage == 'viewed' ) ? 'view' : 'shortlist'
    let tc = ( params.stage == 'viewed' ) ? 0 : 1
    ajaxRequestWithData(
        dave_url + "/object/interaction", 
        'POST', headersfromcookie, 
        JSON.stringify(params),
        function(data) {
            let d = {}
            d[cstage] = 1
            d['wishlist_total_count'] = tc
            iupdate_customer(d, function() {
                if ( callbackFunc && typeof( callbackFunc ) == 'function' ) {
                    callbackFunc(data);
                }
            });
        },
        errorFunc
    )
}

function delete_interaction(interaction_id, callbackFunc, errorFunc, headersfromcookie) {
    headersfromcookie = headersfromcookie || getCookie('authentication')
    ajaxRequestWithData(
        dave_url + "/object/interaction/" + interaction_id, 
        'DELETE', headersfromcookie, 
        JSON.stringify({}),
        function(data) {
            iupdate_customer({'shortlist': -1, 'wishlist_total_count': -1}, function() {
                if ( callbackFunc && typeof( callbackFunc ) == 'function' ) {
                    callbackFunc(data);
                }
            });
        },
        errorFunc
    )
}

function add_product_to_wishlist(product_id, callbackFunc, errorFunc, headersfromcookie) {
    params = {
        "product_id": makeSingle(product_id),
        "stage": "shortlisted"
    };
    add_interaction(params, callbackFunc, errorFunc, headersfromcookie);
}

function remove_product_from_wishlist(product_id, callbackFunc, errorFunc, headersfromcookie) {
    headersfromcookie = headersfromcookie || getCookie('authentication')
    ajaxRequestWithData(
        dave_url + "/objects/interaction", 
        'DELETE', headersfromcookie, 
        JSON.stringify({
            "product_id": product_id,
            "customer_id": headersfromcookie['X-I2CE-USER-ID'],
            "stage": "shortlisted"
        }),
        function(data) {
            iupdate_customer({'shortlist': -1, 'wishlist_total_count': -1}, function() {
                if ( callbackFunc && typeof( callbackFunc ) == 'function' ) {
                    callbackFunc(data);
                }
            });
        },
        errorFunc
    )
}

function is_product_in_wishlist(product_id, callbackFunc, errorFunc, headersfromcookie) {
    headersfromcookie = headersfromcookie || getCookie('authentication')
    params = {
        "product_id": product_id,
        "customer_id": headersfromcookie['X-I2CE-USER-ID'],
        "stage": "shortlisted"
    };
    ajaxRequestWithData(
        dave_url + "/objects/interaction", 
        'GET', headersfromcookie, 
        params,
        function(data) {
            if ( data.total_number >= 1 ) {
                if (callbackFunc && typeof (callbackFunc ) == 'function' ) {
                    callbackFunc(data.data[0].interaction_id);
                }
                return true;
            }
            if (callbackFunc && typeof (callbackFunc ) == 'function' ) {
                callbackFunc(false);
            }
            return false;
        },
        errorFunc
    )
}

function add_view_to_wishlist(callbackFunc, previous_interaction, errorFunc, headersfromcookie) {
    headersfromcookie = headersfromcookie || getCookie('authentication')
    params = {
        "customer_id": headersfromcookie['X-I2CE-USER-ID'],
        "selection": get_url_params(),
        "room_id": room_id,
        "view_id": view_id,
        "room_name": options[room_id]["name"],
        "view_name": options[room_id][view_id]["name"],
        "wishlist_type": options[room_id]["id"] == "_" ? "furniture" : "space"
    };
    ajaxRequestWithData(
        dave_url + "/object/wishlist", 
        'POST', headersfromcookie, 
        JSON.stringify(params),
        function(data) {
            if ( previous_interaction != data.wishlist_id ) {
                iupdate_customer({'wishlist_count': 1, 'wishlist_total_count': 1}, function() {;
                    if (callbackFunc && typeof (callbackFunc) == 'function' ) {
                        callbackFunc(data);
                    }
                });
            } else {
                if (callbackFunc && typeof (callbackFunc) == 'function' ) {
                    callbackFunc(data);
                }
            }
        },
        errorFunc
    )
}

function get_product_wishlist(params, callbackFunc, errorFunc, headersfromcookie) {
    headersfromcookie = headersfromcookie || getCookie('authentication')
    params = params || {
        "_page_size": 10,
        "_page_number": 1
    }
    params["customer_id"] = headersfromcookie['X-I2CE-USER-ID']
    params["stage"] = "shortlisted"
    ajaxRequestWithData(
        dave_url + "/objects/interaction", 
        'GET', headersfromcookie, 
        params,
        function(data) {
            data.data = data.data.map(function(d) {
                d.product_object['interaction_id'] = d.interaction_id;
                return d.product_object
            });
            if ( callbackFunc && typeof ( callbackFunc ) == 'function' ) {
                callbackFunc(data);
            }
        },
        errorFunc
    )
}

function get_wishlist(params, callbackFunc, errorFunc, headersfromcookie) {
    headersfromcookie = headersfromcookie || getCookie('authentication')
    params = params || {
        "_page_size": 10,
        "_page_number": 1
    }
    params["customer_id"] = headersfromcookie['X-I2CE-USER-ID']
    params["wishlist_type"] = params["wishlist_type"] || "space"
    ajaxRequestWithData(
        dave_url + "/objects/wishlist", 
        'GET', headersfromcookie, 
        params,
        function(data) {
            if ( callbackFunc && typeof ( callbackFunc ) == 'function' ) {
                callbackFunc(data);
            }
        },
        errorFunc
    )
}

function get_space_wishlist(params, callbackFunc, errorFunc, headersfromcookie) {
    params["wishlist_type"] = "space"
    get_wishlist(params, callbackFunc, errorFunc, headersfromcookie);
}

function get_furniture_wishlist(params, callbackFunc, errorFunc, headersfromcookie) {
    params["wishlist_type"] = "furniture"
    get_wishlist(params, callbackFunc, errorFunc, headersfromcookie);
}

function delete_wishlist(interaction_id, callbackFunc, errorFunc, headersfromcookie) {
    headersfromcookie = headersfromcookie || getCookie('authentication')
    ajaxRequestWithData(
        dave_url + "/object/wishlist/" + interaction_id, 
        'DELETE', headersfromcookie, 
        JSON.stringify({}),
        function(data) {
            iupdate_customer({'wishlist_count': -1, 'wishlist_total_count': -1}, function() {
                if ( callbackFunc && typeof (callbackFunc) == 'function'  ) {
                    callbackFunc(data);
                }
            });
        },
        errorFunc
    )
}

function is_view_in_wishlist(room_id, view_id, callbackFunc, errorFunc, headersfromcookie) {
    headersfromcookie = headersfromcookie || getCookie('authentication')
    params = {
        "room_id": room_id,
        "customer_id": headersfromcookie['X-I2CE-USER-ID'],
        "view_id": view_id
    };
    ajaxRequestWithData(
        dave_url + "/objects/wishlist", 
        'GET', headersfromcookie, 
        params,
        function(data) {
            if ( data.total_number >= 1 ) {
                if (callbackFunc && typeof (callbackFunc ) == 'function' ) {
                    callbackFunc(data.data[0].wishlist_id);
                }
                return true;
            }
            if (callbackFunc && typeof (callbackFunc ) == 'function' ) {
                callbackFunc(false);
            }
            return false;
        },
        errorFunc
    )
}

function get_designs(params, callbackFunc, errorFunc, headersfromcookie) {
    params = params || {};
    headersfromcookie = headersfromcookie || getCookie('authentication')
    let f = function() {
        ajaxRequestWithData(
            dave_url + "/objects/product", 
            'GET', headersfromcookie, 
            params,
            callbackFunc,
            errorFunc
        ) 
    }
    if (!headersfromcookie) {
        signup({}, f);
        return
    }
    f();
}

function get_design(product_id, callbackFunc, errorFunc, headersfromcookie) {
    headersfromcookie = headersfromcookie || getCookie('authentication')
    let f = function() {
        ajaxRequestWithData(
            dave_url + "/object/product/" + product_id, 
            'GET', headersfromcookie, 
            {},
            callbackFunc,
            errorFunc
        )
    }
    if (!headersfromcookie) {
        signup({}, f);
        return
    }
    f();
}

function get_merinolens_designs(params, callbackFunc, errorFunc, headersfromcookie) {
    params = params || {};
    headersfromcookie = headersfromcookie || getCookie('authentication')
    let f = function() {
        ajaxRequestWithData(
            dave_url + "/objects/merinolens", 
            'GET', headersfromcookie, 
            params,
            callbackFunc,
            errorFunc
        ) 
    }
    if (!headersfromcookie) {
        signup({}, f);
        return
    }
    f();
}

function get_merinolens_design(product_id, callbackFunc, errorFunc, headersfromcookie) {
    headersfromcookie = headersfromcookie || getCookie('authentication')
    let f = function() {
        ajaxRequestWithData(
            dave_url + "/object/merinolens/" + product_id, 
            'GET', headersfromcookie, 
            {},
            callbackFunc,
            errorFunc
        )
    }
    if (!headersfromcookie) {
        signup({}, f);
        return
    }
    f();
}

function get_similar_designs(design_instance, callbackFunc, errorFunc, headersfromcookie) {
    headersfromcookie = headersfromcookie || getCookie('authentication')
    let f = function() {
        ajaxRequestWithData(
            dave_url + "/objects/product", 
            'GET', headersfromcookie, 
            {
                'category_name': design_instance['category_name'],
                'sub_sub_category_name': design_instance['sub_sub_category_name'],
            },
            callbackFunc,
            errorFunc
        ) 
    }
    if (!headersfromcookie) {
        signup({}, f);
        return
    }
    f();
}

function upload_file(data, name, callbackFunc, errorFunc, headersfromcookie) {
    headersfromcookie = headersfromcookie || getCookie('authentication')
    var formData = new FormData();
    formData.append('file', data, name);
    $.ajax({
        url: dave_url + "/upload_file?large_file=true",
        type: "POST",
        dataType: "json",
        contentType: false,
        processData: false,
        headers:{
            "X-I2CE-ENTERPRISE-ID": enterprise_id,
            "X-I2CE-USER-ID": headersfromcookie["X-I2CE-USER-ID"],
            "X-I2CE-API-KEY": headersfromcookie["X-I2CE-API-KEY"]
        },
        data:formData,
        statusCode: {
            401: signup
        }
    }).done(function(data) {
        if (callbackFunc ) {
          callbackFunc(data);
        }
    }).fail(function(err) {
        if (errorFunc) {
            errorFunc(err)
        }
    });
}

function get_total_shortlist(callbackFunc, errorFunc, headersfromcookie) {
    headersfromcookie = headersfromcookie || getCookie('authentication')
    ajaxRequestWithData(
        dave_url + "/object/customer/" + headersfromcookie['X-I2CE-USER-ID'], 
        'GET', headersfromcookie, 
        null, function(data) {
            callbackFunc(data.wishlist_total_count);
        },
    )
}

function iupdate_design(design_id, type, headersfromcookie) {
    let options = {};
    options[type] =  1;
    headersfromcookie = headersfromcookie || getCookie('authentication')
    ajaxRequestWithData(
        dave_url + "/iupdate/product/" + design_id, 
        'PATCH', headersfromcookie, 
        JSON.stringify(options),
    )
}
function iupdate_customer(options, callbackFunc, errorFunc, headersfromcookie) {
    options = options || {};
    headersfromcookie = headersfromcookie || getCookie('authentication')
    let f = function() {
        ajaxRequestWithData(
            dave_url + "/iupdate/customer/" + headersfromcookie['X-I2CE-USER-ID'], 
            'PATCH', headersfromcookie, 
            JSON.stringify(options),
            callbackFunc, 
            errorFunc
        )
    }
    if (!headersfromcookie) {
        signup({}, f);
        return
    }
    f();
}
function login(user_name, password, callbackFunc, errorFunc) {
    let params = {};
    params["user_id"] = user_name;
    params["password"] = password;
    params["roles"] = 'customer';
    params["attrs"] = ['email', 'phone_number'];
    params["enterprise_id"] = enterprise_id;
    DAVE_SETTINGS.ajaxRequestWithData(
        "/dave/oauth",
        "POST",
        JSON.stringify(params),
        function (data) {
            let HEADERS = {
                "Content-Type": "application/json",
                "X-I2CE-ENTERPRISE-ID": DAVE_SETTINGS.ENTERPRISE_ID,
                "X-I2CE-USER-ID": data.user_id,
                "X-I2CE-API-KEY": data.api_key,
            };
            DAVE_SETTINGS.setCookie("authentication", JSON.stringify(HEADERS), 24);
            DAVE_SETTINGS.setCookie("logged_in", true);
            logged_in = true;
        },
        errorFunc,
        unauthorizedFunc
    );
}

function send_otp(number, email, headersfromcookie) {
    setCookie('otp', '');
    headersfromcookie = headersfromcookie || getCookie('authentication')
    let l = generate_random_otp();
    ajaxRequestWithData(
        dave_url + "/transaction/sms", 
        'POST', headersfromcookie, 
        JSON.stringify({
            "recipient": number,
            "message": "Your OTP for Octopus Products is " + l,
        }),
        function() {
            setCookie('otp', l)
        }
    )
    if ( email ) {
        ajaxRequestWithData(
            dave_url + "/transaction/email", 
            'POST', headersfromcookie, 
            JSON.stringify({
                "recipient": email,
                "message": "Your OTP for Octopus Products is " + l,
            })
        )
    }
}

function verify_otp(otp) {
    let g = getCookie('otp');
    if (g && otp && g == otp) {
        return true
    }
    return false
}

function do_download(image_url, ){
    var link = document.createElement('a');
    link.download = `${opts['name']}.jpg`;
    link.href = data;
    link.click();
    link.remove();
}

function alphanumeric(inputtxt) {
    var letterNumber = /^[0-9a-zA-Z_-]+$/;
    if(inputtxt.match(letterNumber)) {
        return true;
    } else { 
        return false; 
    }
}

function validate_email(v) {
    let emailReg = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/i;
    return emailReg.test(v)
}

function getDataUrl(img, canvas) {
    if  ( img.width <= 0 || img.height <= 0 ) {
        return null;
    }
    // Create canvas
    canvas = canvas || document.createElement('canvas');
    let ctx = canvas.getContext('2d');
    // Set width and height
    canvas.width = img.width;
    canvas.height = img.height;
    // Draw the image
    ctx.drawImage(img, 0, 0);
    return canvas.toDataURL('image/jpeg');
}


function validateNumber(evt) {
    var e = evt || window.event;
    var key = e.keyCode || e.which;

    if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
    // numbers   
    key >= 48 && key <= 57 ||
    // Numeric keypad
    key >= 96 && key <= 105 ||
    // + symbol
    key == 107 || key == 43 ||
    // Backspace and Tab and Enter
    key == 8 || key == 9 || key == 13 ||
    // Home and End
    key == 35 || key == 36 ||
    // left and right arrows
    key == 37 || key == 39 ||
    // Del and Ins
    key == 46 || key == 45) {
        // input is VALID
    }
    else {
        // input is INVALID
        e.returnValue = false;
        if (e.preventDefault) e.preventDefault();
    }
}
function once(el, type, fn) {
    function handler(event) {
        el.removeEventListener(type, handler);
        fn(event);
    }
    el.addEventListener(type, handler);
}
function go_back() {
    history.back();
}

function makeSingle(inp, def) {
    if ( inp == undefined || inp == null ) {
        return def;
    }         
    if (Array.isArray(inp)) {
        if ( inp.length == 0 ) {                                                                                                                              
            return def
        }
        return inp[0];
    }
    return inp
}
$.fn.ForcePhoneNumbersOnly = function (disallow_plus) {
    return this.each(function () {
        let that = this;
        $(this).keydown(function (e) {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            if (!disallow_plus) {
                if (  e.shiftKey && key == 187 && that.value.length < 1 ) {
                    return true;
                }
                if ( e.shiftKey && key != 187 ) {
                    return false;
                }
                if ( that.value[0] == '+' && that.value.length > 13 ) {
                    return (
                        key == 8 ||
                        key == 9 ||
                        key == 13 ||
                        key == 46 ||
                        key == 110 ||
                        key == 190 ||
                        (key >= 35 && key <= 40) ||
                        key == 61
                    );
                }
                if ( that.value[0] != '+' && that.value.length > 10 ) {
                    return (
                        key == 8 ||
                        key == 9 ||
                        key == 13 ||
                        key == 46 ||
                        key == 110 ||
                        key == 190 ||
                        (key >= 35 && key <= 40) ||
                        key == 61
                    );
                }
            }
            return (
                key == 8 ||
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105) ||
                key == 61 || key == 107
            );
        });
    });
};
$.fn.setMaxLength = function (length) {
    length = length || 40
    return this.each(function () {
        let that = this;
        $(this).keydown(function (e) {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            if ( that.value.length >= length ) {
                return (
                    key == 8 ||
                    key == 9 ||
                    key == 13 ||
                    key == 46 ||
                    key == 110 ||
                    key == 190 ||
                    (key >= 35 && key <= 40) ||
                    key == 61
                );
            }
            return true
        });
    });
};
 
function file_to_base64(file, callbackFunc) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        console.log(reader.result);
        callbackFunc(reader.result)
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };
}

function base64_to_file(dataURL, filename, type, callbackFunc) {
    fetch(dataURL).then((response) => response.blob()).then((myBlob) => {
        const file = new File([myBlob], filename || 'fileName.jpg', {type: type || "image/jpeg", lastModified:new Date()});
        if ( callbackFunc && typeof(callbackFunc) == 'function' )  {
            callbackFunc(file);
        }
    });
}

function update_selector_results(image, product_id, thumbs_up, thumbs_down, callbackFunc, errorFunc, headersfromcookie) {
    headersfromcookie = headersfromcookie || getCookie('authentication')
    options = {
        "customer_id": headersfromcookie['X-I2CE-USER-ID'],
        "image": image,
        "product_id": product_id,
        "matched": thumbs_up,
        "incorrect": thumbs_down
    };
    let f = function() {
        ajaxRequestWithData(
            dave_url + "/object/prediction_result",  
            'POST', headersfromcookie, 
            JSON.stringify(options),
            callbackFunc, 
            errorFunc
        )
    }
    if (!headersfromcookie) {
        signup({}, f);
        return
    }
    f();
}