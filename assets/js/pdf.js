function img_to_data(elem, callbackFunc, myCanvas, scale) {
    let imagesLoaded = 0;
    let imagesCreated = [];
    elem = elem || "#main";
    myCanvas = myCanvas || "my-canvas";
    scale = scale || 1;
    function loadImage(src, onload) {
        var img = new Image();
        img.onload = onload;
        img.src = src;
        img.crossOrigin = 'anonymous';
        return img;
    }
    function draw(ctx, canvas) {
        imagesLoaded += 1;
        if( imagesLoaded >= imagesCreated.length ) {
            for ( let img1 of imagesCreated ) {
                ctx.drawImage(img1, 0, 0, 1280*scale, 657*scale, 0, 0, $( "#" + myCanvas ).width(), 657*$( "#" + myCanvas ).width()/1280);
            }
            imagesLoaded = 0;
            imagesCreated = [];
            callbackFunc(canvas.toDataURL('image/jpeg'));
        }
    }
    var canvas = document.getElementById(myCanvas);
    var ctx = canvas.getContext("2d");
    //ctx.globalCompositeOperation = 'source-in';
    ctx.globalAlpha = 1;
    $( elem ).css("background-image").split(',').map(
        function(g) {
            r = g.replace(/url\(\"/g, '').replace(/\"\)/g, '').trim();
            imagesCreated.push(loadImage(r, function(){ draw(ctx, canvas) }))
        }
    );
    return canvas.toDataURL('image/jpeg');
}

function download_pdf(rid, vid, selection, elem) {
    selection = selection || {}
    rid = rid || selection.room_id || room_id;
    vid = vid || selection.view_id || view_id;
    elem = elem || "#main";
    if ( $.isEmptyObject( selection ) ) {
        selection = get_url_params();
    }
    let opts = options[rid][vid];
    $( "#mds_loader" ).show();
    $( "#downloadable" ).remove();
    $( 'body' ).append(`
        <div class="container" id="downloadable">
            <div class="pdf-main row">
                <div class="col-md-12 col-sm-12 col-xs-12 pdf-header">
                    <img src="assets/images/mernino_pdf_logo.png"> 
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 pdf-body">
                     <h2 class="text-center m-t-2-rem">${opts['name'].toUpperCase()}</h2>
                     <img id="downloadable-img">
                     <canvas id="my-canvas" width="1280" height="657"></canvas>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 pdf-content"> 
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 pdf-footer"> 
                     <h5>CONTACT US ON : <span class="pr-color">080-69056500</span></h5>
                     <h5 class="bl-color">merinolaminates.com</h5>
                     <p>Note - Digital Rendition may vary from the Original product</p>
                </div>
            </div>
        </div>
        <canvas id="pdf-canvas"></canvas>
    `);
    set_image( init_image_list(rid, vid, selection), $( elem ) );
    all_there = 0;
    all_done = 0;
    for ( const [k, v] of Object.entries(opts) ) {
        if ( k == 'id' || k == 'name' || k == 'background' || !opts[k].variable || !opts ) {
            continue
        }
        let s = selection[k] || opts[k]['init'];
        console.log("Selected design: ", v['surface_id'], ' - ', s)
        let n = opts[k].title;
        let g = $(`
            <h2> <span style="font-weight:400;">${n}</span> <span style="float:right;" class="design-${s}">${s}</span> </h2>
        `);
        all_there += 1;
        get_design(s, function(result) {
            $( ".design-" + result.product_id ).text(result.product_name + ' - ' + result.product_id)
            all_done += 1;
        });
        $( ".pdf-content" ).append(g)
    }
    function wait_until() {
        if ( all_done < all_there ) {
            setTimeout(wait_until, 1000);
            return false
        }
        img_to_data( elem, function(data) {
            $( "#downloadable-img" ).attr('src', data);
            $( "#my-canvas" ).remove();
            html2canvas(document.getElementById("downloadable"), {'useCORS': true, 'y': $( "#downloadable" ).offset()['top'], 'height': $( "#downloadable" ).height() + 140}).then(function(canvas) {
                let pdf_image = canvas.toDataURL("image/jpeg");
                //do_download(pdf_image);
                let doc = new jsPDF('p', 'pt', [canvas.width, canvas.height]);
                doc.addImage(pdf_image,'JPEG',0,0,canvas.width, canvas.height);
                doc.save(`Merino_Digital_Folder_${opts['name']}.pdf`);
                iupdate_customer({"download": 1, "_async": true});
                $( "#mds_loader" ).hide();
                $( "#downloadable" ).remove();
            });
        });
        return true
    }
    wait_until();
}
