let pipeline_url = "https://pipeline.iamdave.ai"

function init_pipeline(callbackFunc, errorFunc) {
    let HEADERS = getCookie('authentication');
    if (!HEADERS) {
        console.log('No authentication available');
        return
    }
    $.ajax({
        url: pipeline_url + '/connect',
        method: "POST",
        dataType: "json",
        contentType: "json",
        headers: {
            'Content-Type': 'application/json'
        },
        data: JSON.stringify({
            user_id: HEADERS['X-I2CE-USER-ID'],
            enterprise_id: HEADERS['X-I2CE-ENTERPRISE-ID'],
            api_key: HEADERS['X-I2CE-API-KEY'],
            host: dave_url,
            pipeline: {
              "resolution": "512x512",
              "id": 1,
              "action": "tasks",
              "tasks": [
                  {
                      "action": "classify",
                      "model": "merino_classifier_512_V5_8.h5"
                  }
              ]
            }
        }),
        success: function(data) {
            setCookie("pipeline_authentication", JSON.stringify({
                "X-I2CE-PIPELINE-ID": data.pipeline_id,
                'Content-Type': 'application/json'
            }), 24);
            if (callbackFunc && typeof( callbackFunc ) == 'function' ) {
                callbackFunc(data);
            }
        },
        error: function(r, e) {
            console.log(e)
            if (errorFunc && typeof ( errorFunc ) == 'function' ) {
                errorFunc(r, e);
            }
        }
    });
}

function predict(image, callbackFunc, errorFunc) {
    let HEADERS = getCookie('pipeline_authentication');
    if (!HEADERS) {
        console.err('No authentication available');
        return
    }
    $.ajax({
        url: pipeline_url + '/run-pipeline',
        method: "POST",
        dataType: "json",
        contentType: "json",
        headers: HEADERS,
        data: JSON.stringify({
              "debug": true,
              "frame_id": "1",
              "src": image,
              "no_predictions": 20
        }),
        success: function(d) {
           check_results(callbackFunc, errorFunc); 
        },
        error: function(r, e) {
            console.log(e)
            if (errorFunc && typeof ( errorFunc ) == 'function' ) {
                errorFunc(r, e);
            }
        }
    });
}
function check_results(callbackFunc, errorFunc) {
    let HEADERS = getCookie('pipeline_authentication');
    if (!HEADERS) {
        console.err('No authentication available');
        return
    }
    $.ajax({
        url: pipeline_url + '/results',
        method: "GET",
        dataType: "json",
        contentType: "json",
        headers: HEADERS,
        success: function(data) {
            if (data.data.length <= 0 ) {
                setTimeout(function() {
                    check_results(callbackFunc, errorFunc)
                }, 1000);
                return
            }
            callbackFunc(data.data)
        },
        error: function(r, e) {
            console.log(e)
            if (errorFunc && typeof ( errorFunc ) == 'function' ) {
                errorFunc(r, e);
            }
        }
    });
}
