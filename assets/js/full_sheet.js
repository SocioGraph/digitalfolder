$( "input" ).focus(function() { 
    $('#orientationModal').fadeOut(); 
}); 

$("#laminate-text").on('click', function(){ 
    $('#orientationModal').fadeOut(); 
});

$("#laminate-text").on('keyup', function(){ 
    if($(this).val().length){
        getDropdownDesign($(this).val())
    }else{
        $("#ul-design-list").html(''); 
    }
});
 
function getDropdownDesign(keyword)
{   
    $("#ul-design-list").html(''); 
    get_designs({'search_term': '~'+keyword , _page_size : 10}, 
    function (result)
    {  
        if(!result.data.length){
            $("#ul-design-list").html(`<li class="text-center">No data found!.</li>`); 
            return false;
        }
        const designs = result.data; 
        let singleDesign = ""; 
        designs.map((design)=>{ 
          singleDesign += `<li onclick="goToDesign('${design.product_id}')">${design.product_name} - ${design.product_id} </li>`;  
        });  
        $("#ul-design-list").html(singleDesign);  
    });
}
function goToDesign(productId)
{ 
    window.location.href='design.html?product_id='+productId;
}
 
//START : Display landscape view modal on screen resize
function getOrientationModal(){
    let newWidth = window.innerWidth;
    let newHeight = window.innerHeight;  
    if(parseInt(newHeight)>parseInt(newWidth)){ 
        $('#orientationModal').fadeOut();
    }else{ 
        $('#orientationModal').fadeIn(); 
    }
} 
getOrientationModal();
window.addEventListener('resize', function(event){
    getOrientationModal(); 
});
//END : Display landscape view modal on screen resize
$(".close_c_modal").on("click", function(){
    $('.modal').fadeOut(); 
});