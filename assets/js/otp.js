var cookie = getCookie('user_details');
var mobile_number = getCookie('mobile_number');

function displayCommonPopup(htmlContent, redirectUri)
{   
    $( "#mds_loader" ).hide();
    $("#otpCommonPopup").fadeIn();
    $(".otpCommonPopupHtml").html(htmlContent);
    if(redirectUri){
        $(".btnCloseOtpCommonPopup").data("redirectUri", redirectUri);
    } 
}

$(".btnCloseOtpCommonPopup").on("click", function(){ 
    const redirectUri = $(this).data("redirectUri");
    if(redirectUri){
        window.location.href = redirectUri;
    }
    $("#otpCommonPopup").fadeOut();
})

function sendOtp() {
    $( "#mds_loader").show();
    $.ajax({
        url: dave_url + "/otp/dave",
        method: "GET",
        dataType: "json",
        contentType: "application/json",
        headers: {
            "Content-Type": "application/json",
            'X-I2CE-ENTERPRISE-ID': 'merino_digitalfolder',
            'X-I2CE-SIGNUP-API-KEY': '9368547c-bd11-3c8e-bfa6-accf9d4a0643'
        },
        data: {
            enterprise_id: "merino_digitalfolder",
            user_id: $("#mobile_number").val().toString() || getCookie('mobile_number'),
            channel: "sms", 
            template: "{otp}",
            provider: 'TwoFactorSender'
        },
        success: function (data) {
            //alert('OTP Sent'); 
            setCookie('otp_validate', data.otp_validate);
            setCookie('mobile_number', $("#mobile_number").val().toString() || data.user_id.toString());
            $( "#mds_loader").show();
            window.location.href = "otp.html"
        },
        error: function(e,r) {
            console.error("Error in sending OTP");
            $( "#mds_loader").hide();
        }
    })
}

function process_validation(msg, login) 
{ 
    if (login) 
    {
        var HEADERS = {
            "Content-Type": "application/json",
            "X-I2CE-ENTERPRISE-ID": enterprise_id,
            "X-I2CE-USER-ID": msg.customer_id,
            "X-I2CE-API-KEY": msg.api_key,
        };
        setCookie("authentication", JSON.stringify(HEADERS), 24);
        setCookie("logged_in", true);
        clearCookie('role');
        clearCookie('user_data');
        clearCookie('otp_validate');
        $( "#mds_loader" ).show();
        logged_in = true; 
        
        /*alert('OTP verified!');
        window.location.href = "match-design.html";*/
        displayCommonPopup('OTP verified! <img src="assets/images/checkmark_1.png" style="height: 19px;width: 20px;margin-top: -5px;">', 'match-design.html'); 
    }else {
        clearCookie('logged_in');
        clearCookie('role');
        clearCookie('authentication');
        clearCookie('pipeline_authentication');
        clearCookie('otp_validate');
        $( "#mds_loader" ).show();
        /*alert(`You have registered with an in-valid mobile number. Please sign-up Again`);
        window.location.href = "sign-up.html"*/
        displayCommonPopup('You have registered with an in-valid mobile number. Please sign-up Again', 'sign-up.html');
    }  
}

function verifyOtp() {
    var otp_no = $('#digit-1').val() + $('#digit-2').val() + $('#digit-3').val() + $('#digit-4').val()
    console.log(otp_no);
    $( "#mds_loader" ).show();
    let data = {
        enterprise_id: "merino_digitalfolder",
        user_id: ""+getCookie('mobile_number'),
        otp_validate: getCookie('otp_validate'),
        otp: otp_no,
        role: 'customer',
        attr: 'mobile_number'
    }
    if (getCookie('user_data')) {
        data['user_data'] = getCookie('user_data');
    }
    $.ajax({
        url: dave_url + "/otp/dave",
        method: "POST",
        dataType: "json",
        contentType: "application/json",
        headers:{ 
            "Content-Type":"application/json",
            'X-I2CE-ENTERPRISE-ID': 'merino_digitalfolder',
            'X-I2CE-SIGNUP-API-KEY': '9368547c-bd11-3c8e-bfa6-accf9d4a0643'},
        data: JSON.stringify(data),
        success: function (msg) {
            console.log("msg : ",msg);
           if (msg.api_key && msg.customer_id ) {
                process_validation(msg, true)
            } else {
                clearCookie('logged_in');
                clearCookie('authentication');
                clearCookie('otp_validate');
                setCookie("mobile_number_verified", true);
                $( "#mds_loader" ).show();
                /*alert('OTP Verified! Please sign up')
                window.location.href = "sign-up.html"*/
                displayCommonPopup('OTP Verified! Please sign up', 'sign-up.html');
            }
            $( "#mds_loader" ).hide();
        },
        error: function (a, b, c) {
            console.error(a.responseText)
            let user_data = getCookie('user_data');
            clearCookie('mobile_number_verified');
            $( "#digit-1" ).val('');
            $( "#digit-2" ).val('');
            $( "#digit-3" ).val('');
            $( "#digit-4" ).val('');
            if (a.responseJSON && a.responseJSON.info && a.responseJSON.info == 'OTP is validated' && user_data) {
                $( "#mds_loader" ).show();
                signup(user_data, function(data) {
                    clearCookie('user_data');
                    process_validation(data);
                });
                return
            } 
            $( "#mds_loader" ).hide(); 
            displayCommonPopup('Incorrect OTP! Try Again', '')
        }

    })
}
function testOtp() {
    var otp_no = $('#digit-1').val() + $('#digit-2').val() + $('#digit-3').val() + $('#digit-4').val()
    console.log(otp_no);
    if (otp_no == '1212') {
        window.location.href = "match-design.html"
    }else{
        history.back()
    }
}
function verifyOtpForLogin() {
    $.ajax({
        url: "https://dashboard.iamdave.ai/otp/dave",
        method: "POST",
        dataType: "json",
        contentType: "application/json",
        headers: {
            'X-I2CE-ENTERPRISE-ID': 'merino_digitalfolder',
            'X-I2CE-SIGNUP-API-KEY': 'ZGF2ZSBleHBvMTU5NzEyNzc0NyA1Ng__'
        },
        data: {
            enterprise_id: "merino_digitalfolder",
            user_id: decodeURIComponent(cookie.mobile_number),
            channel: "sms",
            otp_validate: getCookie('otp_validate'),
            otp: otp_no,
            role: 'customer',
        },
        success: function (msg) {
            alert('OTP Sent');
            console.log(msg);
        }
    })
}
