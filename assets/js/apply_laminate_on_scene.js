$.mobile.autoInitializePage = false;
const parameters = get_url_params();
const productId = parameters['product_id']; 
const urlRoomId = parameters['room_id']; 

let orntModalHeader = `Use Landscape View`;
let orntModalbody = `<img src="assets/images/orientation_1.webp" class="mb-15 w-100-p"><p>Please rotate your device to get better view of the visualizer.</p>`;
 
if (productId) {
    get_design(productId, function (design){ 
        let pageHeader = design.product_name + ' Apply Laminate on Scene';
        document.title = pageHeader;  
    });
} else {
        let pageHeader = 'Apply Laminate on Scene';
        document.title = pageHeader;  
}

//Suede and Gloss filter
let finish = $("input[type=radio][name='finish']:checked").val()
$('input:radio[name="finish"]').change(function(){
    finish = $(this).val();
    if(finish==='Suede'){
        $(".finish-suede").addClass("selected-finish"); 
        $(".finish-gloss").removeClass("selected-finish");  
    }else{
        $(".finish-gloss").addClass("selected-finish"); 
        $(".finish-suede").removeClass("selected-finish");
    }

    let filterCategory = localStorage.getItem('filterCategory');
    let filterSubCategory = localStorage.getItem('filterSubCategory');
    if(filterCategory || filterSubCategory){
        applyFilter(filterCategory, filterSubCategory)
    }else{
        getBestSellingDesigns(finish);
    } 
});
  
//START : Filter 
function applyFilter(category, subCategory, page_number=1)
{
    closeSearchFilteredResults();
    localStorage.setItem('filterCategory', category);
    localStorage.setItem('filterSubCategory', subCategory);
 
    if(page_number==1){
        $('.best_selling_design_loader').show(); //Show loader
        $('.mds_viewed_slider , .mds_mobile_best_selling_designs').html(""); // Remove old designs  
    }else{
        $(".loadMorePortrait , .loadMoreLandscape").html(`<div class="col-md-12 text-center"><img src="assets/images/loader.gif" style="width: 20px;"></div>`); //Load more loader
    }
    $('.land-filter-dropdowns , .dropdown-menu-sub').hide(); // Hide filter dropdown

    $(".best-selling-header").html(subCategory + ' Designs');

    /*$(".viz-direct-link").attr('href', 'viz_best_selling.html?sub_category=' + subCategory); 
    let viz_link = 'viz_best_selling.html?sub_category=' + subCategory*/

    if(typeof subCategory === "undefined" || !subCategory || subCategory=='' || subCategory == "undefined"){

        $(".best-selling-header").html(category + ' Designs');
        if(category=='New Designs'){
            $(".best-selling-header").html('New Designs');
        }
        
        /*$(".viz-direct-link").attr('href', 'viz_best_selling.html?category=' + category);
        viz_link = 'viz_best_selling.html?category=' + category*/
 
        get_designs({ 'finishes': finish, category_name: category, _page_size : 10, _page_number: page_number}, function (result){  
            displayBestSellingDesigns(result);  
        });
    }else{ 
        get_designs({ 'finishes': finish, category_name: category, sub_category_name: subCategory, _page_size : 10, _page_number: page_number}, function (result){  
            displayBestSellingDesigns(result);  
        });
    }
}
  
//END : Filter


//START : Best selling design
function getBestSellingDesigns(finish, page_number=1)
{ 
    if(page_number==1){ 
        $('.best_selling_design_loader').show();  //Display loader 
    }else{   
        $(".loadMorePortrait , .loadMoreLandscape").html(`<div class="col-md-12 text-center"><img src="assets/images/loader.gif" style="width: 20px;"></div>`); //Load more loader
    }

    //$('.mds_viewed_slider , .mds_mobile_best_selling_designs').html(""); // Remove old designs  

    

    localStorage.setItem('filterCategory', '');
    localStorage.setItem('filterSubCategory', '');
    /*let viz_link = 'viz_best_selling.html?best_seller=true';*/
    get_designs({'best_seller': true, 'finishes': finish, _page_size : 10, _page_number: page_number}, function (result){ 
        displayBestSellingDesigns(result); 
    });
}
getBestSellingDesigns(finish);
//END : Best selling design


//START : Display Best selling design
function displayBestSellingDesigns(result) 
{   
    let {is_first, total_number,page_size,page_number,is_last,data:designs} = result; 
    if(is_last){ $(".loadMorePortrait , .loadMoreLandscape").html(""); }
    if(page_number==1){ $(".mds_mobile_best_selling_designs , .mds_viewed_slider").html(""); }

    let singleDesign = singleMobileDesign = '';
    designs.map((design)=>{
        singleDesign += ` 
            <div class="item">  
                <a href="javascript:void(0)" class="t-d-none" onclick="callChange('${design.product_id}', '${design.application_surface.join(",")}', this)">
                    <div class="mds_viewed_image">
                        <img src="${design.thumbnail}" style="width:100%;height:110px;border: 1px solid #33333321;" title="Design ${design.product_id} - ${design.product_name}" alt="Design ${design.product_id} - ${design.product_name}">
                    </div>
                    <p class="t-d-none" title="Design ${design.product_id} - ${design.product_name}">Design ${design.product_id} - ${design.product_name}</p>
                </a>
            </div>`;    
    }); 
    $('.best_selling_design_loader').hide(); //Hide loader 
    if(!singleDesign){
        $(".mds_mobile_best_selling_designs , .mds_viewed_slider").html('<p class="text-center">No data found!.</p>');
    }else{
        
        let portraitDesigns = singleDesign;
        let landscapeDesigns = singleDesign;
 
        $(".loadMorePortrait , .loadMoreLandscape").remove();

        //Portrait View
        portraitDesigns += `<div id="loadMorePortrait" class="item loadMorePortrait"></div>`;
        $(".mds_viewed_slider").append(portraitDesigns);
 
        //Landscape View
        landscapeDesigns +=  `<div id="loadMoreLandscape" class="item loadMoreLandscape"></div>`;

        $(".mds_mobile_best_selling_designs").append(landscapeDesigns);

        if(!is_last){

            let callFunction = '';   
            let filterCategory = localStorage.getItem('filterCategory');
            let filterSubCategory = localStorage.getItem('filterSubCategory');

            if(filterCategory || filterSubCategory){ 
                callFunction = `applyFilter('${filterCategory}', '${filterSubCategory}', ${++page_number})`;
            }else{ 
                callFunction = `getBestSellingDesigns('${finish}', ${++page_number})`;
            }

            let loadMoreBtn = `<div class="col-md-12 text-center"><a href="javascript:void(0)" onclick="${callFunction}" class="btn btn-default" style="padding: 2px 6px;">Load More</a></div>`;
           $(".loadMorePortrait , .loadMoreLandscape").html(loadMoreBtn);
        }

    }
    show_help_text();
}
//END : Display Best selling design

//START : To get filter dropdown from product hierarchy 
const getDropdownFilters = async () => {
    try {
        const filterDropdownApiResult = await fetch("assets/json/productCategoryHierarchy.json");
        filterDropdowns = await filterDropdownApiResult.json(); 
        let landscapeViewFilter = "";
        let portraitViewFilter = "";
        $(".land-filter-dropdowns, .mds_porttrait_view_filter").html('');
        filterDropdowns.map((filter)=>{
             
            let subCategories = filter.sub_categories;
            let htmlSubCategories = "";
            let htmlPortraitSubCategories = "";
            let randomString = generate_random_string(6);
             
            if(subCategories)
            {
                //If sub category  exist, display dropdown
                subCategories.map((subFilter)=>{
                    htmlSubCategories += `   
                        <li><a href="javascript:void(0)" onclick="applyFilter('${filter.category_name}','${subFilter.sub_category_name}')">${subFilter.sub_category_name}</a></li>     
                    `; 
                });

                landscapeViewFilter += `  
                  <div class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle"  data-toggle="dropdown">${filter.category_name}
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                         ${htmlSubCategories}
                    </ul>
                </div>
                `;

                portraitViewFilter = `<li class="pd-0 l-s"> 
                      <a class="dp-${randomString} pd-0 btn_design" href="javascript:void(0)">${filter.category_name}</a> 
                      <div class="dropdown-menu-sub dp-common-hide dp-m-${randomString}"  style="position: absolute !important;left: 30%;"> 
                        <div class="cl-di-dp text-center"><a href="javascript:void(0)" onclick="close_dp('${randomString}')">&times;</a></div>
                        <ul class="dropdown-menu-1">
                            ${htmlSubCategories}
                        </ul> 
                    </div> 
                </li>`;



                $(".mds_porttrait_view_filter").append(portraitViewFilter);
                generateToggleFunction(randomString);  
            }else{
                //If sub category doesn't exist, do not display dropdown
                landscapeViewFilter += `  
                  <div class="dropdown">
                    <a href="javascript:void(0)" onclick="applyFilter('${filter.category_name}')">${filter.category_name}</a> 
                </div>
                `;  
                portraitViewFilter = `<li class="pd-0 l-s"> 
                      <a class="btn_design pd-0" href="javascript:void(0)" onclick="applyFilter('${filter.category_name}')">${filter.category_name}</a>  
                </li>`;
                $(".mds_porttrait_view_filter").append(portraitViewFilter);
            }
        }); 
        $(".land-filter-dropdowns").html(landscapeViewFilter);
        
        $(".btn_design").on("click", function(){ 
            $(".btn_design").removeClass("active");
            $(this).addClass("active");
        })

    } catch (error) {
        console.log("Error : ", error);
    }
}



getDropdownFilters(); 

//END : To get filter dropdown from product hierarchy 


function close_dp(randomString) 
{
    $(".btn_design").removeClass("active");
    $(".dp-m-"+randomString).hide();  
} 

function generateToggleFunction(randomString)
{ 
    $(".dp-"+randomString).on("click", function(){
        $(".dp-common-hide").hide();
        $(".dp-m-"+randomString).toggle();   
        /*console.log(":visible : ", $(".dp-m-"+randomString).is(":visible"));
        if($(".dp-m-"+randomString).is(":visible")){
            $(".dp-m-"+randomString).hide();
        }else{ 
            $(".dp-m-"+randomString).show();
        }*/
    });
}

$(".btn-share-design").on("click", function(){
    if ( detectMob() ) {
        $( ".whatsAppShare" ).attr('href', "whatsapp://send?text=Please click this link to have a look at the designs I have selected: " + encodeURIComponent(window.location.href))
    } else {
        $( ".whatsAppShare" ).attr('href', "https://web.whatsapp.com/send?text=Please click this link to have a look at the designs I have selected: " + encodeURIComponent(window.location.href))
    }
    $( ".share-design" ).show(10);
});

$(".close-share-block").on("click", function(){
    $(".share-design").hide(10);
});
 
 

//Apply change filter 
function callChange(product_id, application, that, f){
    if ( that ) {
        $( ".mds_viewed_image.active" ).removeClass('active');
        $( that ).children('div').addClass('active')
    }
    let r = change(product_id, f || finish, application);
    if (!r) {
        orntModalHeader = `Merino laminates says.`;
        orntModalbody = `This laminate is not applicable on the selected surface.`;
        $("#orntModalHeader").html(orntModalHeader); $("#orntModalbody").html(orntModalbody);
        $('#orientationModal').fadeIn()
        return r
    }
    $( ".w-wish" ).show();
    $( ".w-added" ).hide();
}

//START : Display landscape view modal on screen resize
function getOrientationModal(){
    let newWidth = window.innerWidth;
    let newHeight = window.innerHeight;  
    if(parseInt(newHeight)>parseInt(newWidth)){ 
        //Portrait view
        $('body').removeClass('mb-5').addClass('mb-50');
        if(!localStorage.getItem('isFirstVisit')){
            $("#orntModalHeader").html(orntModalHeader); $("#orntModalbody").html(orntModalbody);
            $('#orientationModal').fadeIn();
            localStorage.setItem('isFirstVisit', true);
        }
    }else{ 
        //landscape view
        if ( $( '#orientationModal' ).is(":visible") ) {
            $('#orientationModal').fadeOut(1000, function() {
                $("html, body").animate({ scrollTop: $(document).height() }, 500);
                show_help_text();
            });
        } else {
            $("html, body").animate({ scrollTop: $(document).height() }, 500);
            show_help_text();
        }
        $('body').removeClass('mb-50').addClass('mb-5');
    }
} 

function show_help_text(force) {
    let newWidth = window.innerWidth;
    let newHeight = window.innerHeight;
    function show(portrait) {
        if ( $( ".mds_viewed_image" ).length <= 0 ) {
            setTimeout(function(){
                show(portrait);
            }, 200);
            return;
        }
        show_sprites();
        helpOverlay.remove();
        setTimeout(function(){
            helpOverlay.remove();
        }, 5000);
        $( ".best-selling-header" ).each(function() {
            helpOverlay.addLabelTo($( this ),"BEST SELLING DESIGNS", "topright", 1, 20, 20);
        });
        helpOverlay.addLabelTo(
            $( $( "#main img").toArray().filter(function(s1) { if ( parseInt($( s1 ).css('left').replace(/px/g, '')) < ($( "#main" ).width()/2) &&  parseInt($( s1 ).css('top').replace(/px/g, '')) <= ($( "#main" ).height() - 30) ) { return true } })[0] ),
        "SELECT A SURFACE", "bottomright", 1, 20, 10);
        if (portrait) {
            helpOverlay.addLabelTo($( ".mds_viewed_slider .mds_viewed_image" ).first(),"APPLY DESIGN", "bottomright", 1, 50, 50);
        } else {
            helpOverlay.addLabelTo($( ".mds_mobile_best_selling_designs .mds_viewed_image" ).first(),"APPLY DESIGN", "bottomright", 1, 50, 50);
        }
    }
    if ( parseInt(newHeight) < parseInt(newWidth) ) {
        if (!getCookie('helpTextLandScape')) {
            show();
            setCookie('helpTextLandScape', true);
        }
    } else if (force){ 
        if (!getCookie('helpTextPortrait')) {
            show(true);
            setCookie('helpTextPortrait', true);
        }
    }
}
getOrientationModal();

function set_up_other_views(inp) {
    for ( let i of inp ) {
        $( ".more-rooms-dots" ).append(`
        <a href="#" class="dot${i.active ? ' active' : ''}" data-view_id="${i.view_id}" data-room_id="${i.room_id}"><span></span></a>
        `);
    }
    $( ".more-rooms-dots .dot" ).on("click", function() {
        load_other_view($( this ).data('view_id'), $( this ).data('room_id'));
    });
    $( "#main" ).on("swiperight", function() {
        let n = $( ".dot.active" ).prev()
        if (n.length) {
            $( n ).click();
        }
    });
    $( "#main" ).on("swipeleft", function() {
        let n = $( ".dot.active" ).next()
        if (n.length) {
            $( n ).click();
        }
    });
}

window.addEventListener('resize', function(event){
    getOrientationModal(); 
});

$("#lock-landscape-button").on("click", function(){
    orntModalHeader = `Use Landscape View`;
    orntModalbody = `<img src="assets/images/orientation_1.webp" class="mb-15 w-100-p"><p>Please rotate your device to get better view of the visualizer.</p>`;
    $("#orntModalHeader").html(orntModalHeader); $("#orntModalbody").html(orntModalbody);
    $('#orientationModal').fadeIn();
});

//END : Display landscape view modal on screen resize

 
$(".btn-landcape-filter").on("click", function(){
    $(".land-filter-dropdowns").toggle();
});

function carouselSlider()
{
   /* if($('.mds_viewed_slider').length)
    {
        var viewedSlider = $('.mds_viewed_slider'); 
        viewedSlider.owlCarousel(
        {
            loop:true,
            margin:30,
          /*  autoplay:true,
            autoplayTimeout:6000,
            nav:false,
            dots:false,
            responsive:
            {
              0:{items:3},
              575:{items:4},
              768:{items:3},
              991:{items:4},
              1199:{items:6}
            }
        }); 
    } */
}
function update_total_shortlist(count) {
   if ( $( "#wishlistCount" ).is(":visible") ) {
        count = parseInt($( "#wishlistCount" ).text()) + count;
   }
   if ( count <= 0 ) {
       $( "#wishlistCount" ).hide();
   } else {
       $( "#wishlistCount" ).show();
       $( "#wishlistCount" ).text(count);
    }
}

$(document).ready(function() { 
    $(".viz-direct-link").attr('href', 'viz_best_selling.html?best_seller=true');
    //carouselSlider();
    is_view_in_wishlist(makeSingle(get_url_params()['room_id']), makeSingle(get_url_params()['view_id']), function(interaction_id){
        $( ".w-added" ).first().data('interaction_id', interaction_id);
    });
    $( ".wishlist" ).on("click", function() {
        $( "#mds_loader" ).show();
        let that = $( this );
        if ( $( ".w-wish" ).is(":visible") ) {
            add_view_to_wishlist(function(data) {
                $( that ).find(".w-wish" ).hide();
                $( that ).find(".w-added" ).show();
                if ( data.wishlist_id != $( that ).find(".w-added" ).first().data('interaction_id') ) {
                    update_total_shortlist(1);
                }
                $( that ).find(".w-added" ).first().data('interaction_id', data.wishlist_id);
                $( "#mds_loader" ).hide();
            }, $( that ).find(".w-added" ).first().data('interaction_id') );
        } else {
            delete_wishlist($( that ).find(".w-added").first().data('interaction_id'), function() {
                $( that ).find(".w-wish" ).show();
                $( that ).find(".w-added" ).hide();
                update_total_shortlist(-1);
                $( "#mds_loader" ).hide();
            });
        }
    });
    get_total_shortlist(update_total_shortlist);
    $( ".viz-ic-download" ).on("click", function() {download_pdf()})
});

$(".close_c_modal").on("click", function(){
  $('.modal').fadeOut(); 
  show_help_text(true);
});

function generate_random_string(string_length){
    let random_string = '';
    let random_ascii;
    for(let i = 0; i < string_length; i++) {
        random_ascii = Math.floor((Math.random() * 25) + 97);
        random_string += String.fromCharCode(random_ascii);
    }
    return random_string;
}
