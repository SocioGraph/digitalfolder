rooms: [
    {
      "id": "master_bedroom",
      "title": "Master Bedroom",
      "thumbnail": "/assets/images/2dviz/bedroom/master_bedroom.jpg",
      "views": [
          {
              "id": "master_view",
              "title": "Master View",
              "thumbnail": "/assets/images/2dviz/bedroom/master_bedroom.jpg",
              "surfaces": [
                  {
                    "name": "holed_out",
                    "variable": false,
                    "init": "/assets/images/2dviz/bedroom/holed_out.webp",
                  },  
                  {
                    "name": "wall_cladding_bed_side_table",
                    "variable": true,
                    "left": "calc( 0.58*100vw )",
                    "top": "calc( 0.26*0.67*100vw )",
                    "init": "/assets/images/2dviz/bedroom/wall_cladding_bed_side_table.jpg",
                    "options": [],
                  },
                  {
                    "name": "wardrobe_door_side_table_drawers",
                    "variable": true,
                    "left": "calc( 0.58*100vw )",
                    "top": "calc( 0.26*0.67*100vw )",
                    "init": "/assets/images/2dviz/bedroom/wardrobe_door_side_table_drawers.jpg",
                    "options": [],
                  },
                  {
                    "name": "wardrobe_wardrobe_inner",
                    "variable": true,
                    "left": "calc( 0.58*100vw )",
                    "top": "calc( 0.26*0.67*100vw )",
                    "init": "/assets/images/2dviz/bedroom/wardrobe_wardrobe_inner.jpg",
                    "options": [],
                  }
              ]
          },
          {
              "id": "tv_unit",
              "title": "TV Unit",
              "thumbnail": "/assets/images/2dviz/bedroom/tv_unit.jpg",
              "surfaces": [
                  {
                    "name": "holed_out",
                    "variable": false,
                    "init": "/assets/images/2dviz/bedroom/holed_out.webp",
                  },  
                  {
                    "name": "study_unit",
                    "variable": true,
                    "left": "calc( 0.58*100vw )",
                    "top": "calc( 0.26*0.67*100vw )",
                    "init": "/assets/images/2dviz/bedroom/study_unit.jpg",
                    "options": [],
                  },
                  {
                    "name": "wall_cladding",
                    "variable": true,
                    "left": "calc( 0.58*100vw )",
                    "top": "calc( 0.26*0.67*100vw )",
                    "init": "/assets/images/2dviz/bedroom/wall_cladding.jpg",
                    "options": [],
                  } 
              ]
          },
          {
              "id": "view2",
              "title": "TV Unit",
              "thumbnail": "/assets/images/2dviz/bedroom/tv_unit.jpg",
              "surfaces": [
                  {
                    "name": "holed_out",
                    "variable": false,
                    "init": "/assets/images/2dviz/bedroom/holed_out.webp",
                  },   
                  {
                    "name": "wardrobe_wardrobe_inner",
                    "variable": true,
                    "left": "calc( 0.58*100vw )",
                    "top": "calc( 0.26*0.67*100vw )",
                    "init": "/assets/images/2dviz/bedroom/wardrobe_wardrobe_inner.jpg",
                    "options": [],
                  },
                  {
                    "name": "wardrobe_door",
                    "variable": true,
                    "left": "calc( 0.58*100vw )",
                    "top": "calc( 0.26*0.67*100vw )",
                    "init": "/assets/images/2dviz/bedroom/wardrobe_door.jpg",
                    "options": [],
                  } 
              ]
          }
      ]
    },
    {
      "id": "kids_bedroom",
      "title": "Kids Bedroom",
      "thumbnail": "/assets/images/2dviz/bedroom/kids_bedroom.jpg",
      "views": [
          {
              "id": "view1",
              "title": "Master View",
              "thumbnail": "/assets/images/2dviz/bedroom/kids_bedroom.jpg",
              "surfaces": [
                  {
                    "name": "holed_out",
                    "variable": false,
                    "init": "/assets/images/2dviz/bedroom/holed_out.webp",
                  },  
                  {
                    "name": "wall_cladding_bed_side_table",
                    "variable": true,
                    "left": "calc( 0.58*100vw )",
                    "top": "calc( 0.26*0.67*100vw )",
                    "init": "/assets/images/2dviz/bedroom/wall_cladding_bed_side_table.jpg",
                    "options": [],
                  },
                  {
                    "name": "wardrobe_door_side_table_drawers",
                    "variable": true,
                    "left": "calc( 0.58*100vw )",
                    "top": "calc( 0.26*0.67*100vw )",
                    "init": "/assets/images/2dviz/bedroom/wardrobe_door_side_table_drawers.jpg",
                    "options": [],
                  },
                  {
                    "name": "wardrobe_wardrobe_inner",
                    "variable": true,
                    "left": "calc( 0.58*100vw )",
                    "top": "calc( 0.26*0.67*100vw )",
                    "init": "/assets/images/2dviz/bedroom/wardrobe_wardrobe_inner.jpg",
                    "options": [],
                  }
              ]
          },
          {
              "id": "view2",
              "title": "TV Unit",
              "thumbnail": "/assets/images/2dviz/bedroom/tv_unit.jpg",
              "surfaces": [
                  {
                    "name": "holed_out",
                    "variable": false,
                    "init": "/assets/images/2dviz/bedroom/holed_out.webp",
                  },  
                  {
                    "name": "study_unit",
                    "variable": true,
                    "left": "calc( 0.58*100vw )",
                    "top": "calc( 0.26*0.67*100vw )",
                    "init": "/assets/images/2dviz/bedroom/study_unit.jpg",
                    "options": [],
                  },
                  {
                    "name": "wall_cladding",
                    "variable": true,
                    "left": "calc( 0.58*100vw )",
                    "top": "calc( 0.26*0.67*100vw )",
                    "init": "/assets/images/2dviz/bedroom/wall_cladding.jpg",
                    "options": [],
                  } 
              ]
          },
          {
              "id": "view2",
              "title": "TV Unit",
              "thumbnail": "/assets/images/2dviz/bedroom/tv_unit.jpg",
              "surfaces": [
                  {
                    "name": "holed_out",
                    "variable": false,
                    "init": "/assets/images/2dviz/bedroom/holed_out.webp",
                  },   
                  {
                    "name": "wardrobe_wardrobe_inner",
                    "variable": true,
                    "left": "calc( 0.58*100vw )",
                    "top": "calc( 0.26*0.67*100vw )",
                    "init": "/assets/images/2dviz/bedroom/wardrobe_wardrobe_inner.jpg",
                    "options": [],
                  },
                  {
                    "name": "wardrobe_door",
                    "variable": true,
                    "left": "calc( 0.58*100vw )",
                    "top": "calc( 0.26*0.67*100vw )",
                    "init": "/assets/images/2dviz/bedroom/wardrobe_door.jpg",
                    "options": [],
                  } 
              ]
          }
      ]
    },
]